# Linux Hardware Compatibility

## Hardware testato da utenti Debian GNU/Linux

### Per inviare il proprio report, fare un commit o inviare un messaggio con le seguenti informazioni

1. **Tipo dispositivo**: (es: potatile, multifunzione, ecc.)
2. **Nome dispositivo**: nome esatto e completo
3. **Sito internet ufficiale**: pagina del dispositivo sul sito della casa madre
4. **Personalizzazioni**: eventuali personalizzazioni hardware effettuate
5. **Non funziona**: elenco delle parti non funzionanti correttamente
6. **Versione Debian**: versione testata (es: Debian 11, stable, main) o (es: Debian 10, testing, main contrib non-free)
7. **Drivers extra**: divers extra repository ufficiale
8. **Componenti hardware**: elencati tramite i seguenti comandi da terminale (in caso sia un pc)
    - `lspci -nn`
    - `lsusb`
9. **Note**: aggiungere eventuali note

#### Avvertenze

Si rende noto che, invii caotici o che non seguano lo schema sopra precisato, non saranno tenuti in considerazione.
Esempio reale disponibile [qui](../Portatili/DELL%20Precision%20M4800) e [qui](../Multifunzione/HP%20Color%20LaserJet%20Pro%20M281fdw).

#### Template

Saricate il [template](template.txt) per inviare la vostra configurazione.
